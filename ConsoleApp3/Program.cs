﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {

            string LiczSrednia()
            {
                string ocena;
                StringBuilder sb = new StringBuilder();
                do
                {
                    Console.WriteLine("Podaj ocene: ");
                    ocena = Console.ReadLine();
                    sb.Append(ocena + ",");
              
                }
                while (!String.IsNullOrWhiteSpace(ocena));
                return sb.ToString();
            }

            string oceny = LiczSrednia();
            string[] grades = oceny.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            float[] ocenyInt = new float[grades.Length];

            int i = 0;
            foreach(string x in grades)
            {
                ocenyInt[i] = float.Parse(x);
                i++;
            }

            int iloscOcen = 0;
            float sumaOcen = 0;
            foreach (int x in ocenyInt)
            {
                iloscOcen += 1;
                sumaOcen += x;
            }
            //float srednia = sumaOcen/iloscOcen;
            Funkcje funkcje = new Funkcje();
            Console.WriteLine(funkcje.obliczSrednia(sumaOcen, iloscOcen));

            Console.Read();

        }
    }
}